package com.zuitt;

import jakarta.servlet.http.HttpServlet;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1804323570948502389L;

	
	public void init() throws ServletException {   
        System.out.println("******************************************");
        System.out.println(" UserServlet has been initialized. ");
        System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		//The POST request here gets/passes the form data
        String fname = req.getParameter("fname");
        String lname = req.getParameter("lname");
        String email = req.getParameter("email");
        String contactNumber = req.getParameter("contactNumber");

        
        //This stores the First Name to the System Properties
        System.getProperties().put("fname",fname);
        
        
        //This stores the Last Name in HttpSession
        HttpSession session = req.getSession();
        session.setAttribute("lname", lname);
        
        
        //This stores the Email in the Servlet Context setAttribute method
        getServletContext().setAttribute("email", email);

        
        //This stores the Contact Number in the Url Rewriting via sendRedirect Method
        res.sendRedirect("details?contactNumber="+contactNumber);
		
	}
	
	public void destroy() {

		System.out.println("*********************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println("*********************************");
		
	}


}